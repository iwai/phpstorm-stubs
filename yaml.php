<?php
/**
 * @link http://php.net/manual/en/yaml.constants.php
 */
define('YAML_ANY_SCALAR_STYLE', 0);

/**
 * @link http://php.net/manual/en/yaml.constants.php
 */
define('YAML_PLAIN_SCALAR_STYLE', 1);

/**
 * @link http://php.net/manual/en/yaml.constants.php
 */
define('YAML_SINGLE_QUOTED_SCALAR_STYLE', 2);

/**
 * @link http://php.net/manual/en/yaml.constants.php
 */
define('YAML_DOUBLE_QUOTED_SCALAR_STYLE', 3);

/**
 * @link http://php.net/manual/en/yaml.constants.php
 */
define('YAML_LITERAL_SCALAR_STYLE', 4);

/**
 * @link http://php.net/manual/en/yaml.constants.php
 */
define('YAML_FOLDED_SCALAR_STYLE', 5);

/**
 * @link http://php.net/manual/en/yaml.constants.php
 */
define('YAML_NULL_TAG', 'tag:yaml.org,2002:null');

/**
 * @link http://php.net/manual/en/yaml.constants.php
 */
define('YAML_BOOL_TAG', 'tag:yaml.org,2002:bool');

/**
 * @link http://php.net/manual/en/yaml.constants.php
 */
define('YAML_STR_TAG', 'tag:yaml.org,2002:str');

/**
 * @link http://php.net/manual/en/yaml.constants.php
 */
define('YAML_INT_TAG', 'tag:yaml.org,2002:int');

/**
 * @link http://php.net/manual/en/yaml.constants.php
 */
define('YAML_FLOAT_TAG', 'tag:yaml.org,2002:float');

/**
 * @link http://php.net/manual/en/yaml.constants.php
 */
define('YAML_TIMESTAMP_TAG', 'tag:yaml.org,2002:timestamp');

/**
 * @link http://php.net/manual/en/yaml.constants.php
 */
define('YAML_SEQ_TAG', 'tag:yaml.org,2002:seq');

/**
 * @link http://php.net/manual/en/yaml.constants.php
 */
define('YAML_MAP_TAG', 'tag:yaml.org,2002:map');

/**
 * @link http://php.net/manual/en/yaml.constants.php
 */
define('YAML_PHP_TAG', '!php/object');

/**
 * @link http://php.net/manual/en/yaml.constants.php
 */
define('YAML_MERGE_TAG', 'tag:yaml.org,2002:merge');

/**
 * @link http://php.net/manual/en/yaml.constants.php
 */
define('YAML_BINARY_TAG', 'tag:yaml.org,2002:binary');

/**
 * @link http://php.net/manual/en/yaml.constants.php
 */
define('YAML_ANY_ENCODING', 0);

/**
 * @link http://php.net/manual/en/yaml.constants.php
 */
define('YAML_UTF8_ENCODING', 1);

/**
 * @link http://php.net/manual/en/yaml.constants.php
 */
define('YAML_UTF16LE_ENCODING', 2);

/**
 * @link http://php.net/manual/en/yaml.constants.php
 */
define('YAML_UTF16BE_ENCODING', 3);

/**
 * @link http://php.net/manual/en/yaml.constants.php
 */
define('YAML_ANY_BREAK', 0);

/**
 * @link http://php.net/manual/en/yaml.constants.php
 */
define('YAML_CR_BREAK', 1);

/**
 * @link http://php.net/manual/en/yaml.constants.php
 */
define('YAML_LN_BREAK', 2);

/**
 * @link http://php.net/manual/en/yaml.constants.php
 */
define('YAML_CRLN_BREAK', 3);


/**
 * Convert all or part of a YAML document stream to a PHP variable.
 * 
 * @link http://php.net/manual/en/functions.yaml-parse.php
 * @param string $input The string to parse as a YAML document stream
 * @param int $pos Document to extract from stream (-1 for all documents, 0 for
 *        first document, ...)
 * @param int $ndocs If $ndocs is provided, then it is filled with the number of
 *        documents found in stream
 * @param array $callbacks Content handlers for YAML nodes. Associative array of
 *        YAML tag = callable mappings. See parse callbacks for more details
 * @return mixed Returns the value encoded in $input in appropriate PHP type. If $pos is -1
 *         an array will be returned with one entry for each document found in the stream
 */
function yaml_parse($input, $pos = 0, &$ndocs = null, array $callbacks = null) {}

/**
 * Convert all or part of a YAML document stream read from a file to a PHP variable.
 * 
 * @link http://php.net/manual/en/functions.yaml-parse-file.php
 * @param string $filename Path to the file
 * @param int $pos Document to extract from stream (-1 for all documents, 0 for
 *        first document, ...)
 * @param int $ndocs If $ndocs is provided, then it is filled with the number of
 *        documents found in stream
 * @param array $callbacks Content handlers for YAML nodes. Associative array of
 *        YAML tag = callable mappings. See parse callbacks for more details
 * @return mixed Returns the value encoded in $input in appropriate PHP type. If $pos is -1
 *         an array will be returned with one entry for each document found in the stream
 */
function yaml_parse_file($filename, $pos = 0, &$ndocs = null, array $callbacks = null) {}

/**
 * Convert all or part of a YAML document stream read from a URL to a PHP variable.
 * 
 * @link http://php.net/manual/en/functions.yaml-parse-url.php
 * @param string $url $url should be of the form "scheme://...". PHP will search
 *        for a protocol handler (also known as a wrapper) for that scheme. If no wrappers
 *        for that protocol are registered, PHP will emit a notice to help you track
 *        potential problems in your script and then continue as though filename specifies
 *        a regular file
 * @param int $pos Document to extract from stream (-1 for all documents, 0 for
 *        first document, ...)
 * @param int $ndocs If $ndocs is provided, then it is filled with the number of
 *        documents found in stream
 * @param array $callbacks Content handlers for YAML nodes. Associative array of
 *        YAML tag = callable mappings. See parse callbacks for more
 * @return mixed Returns the value encoded in $input in appropriate PHP type. If $pos is -1
 *         an array will be returned with one entry for each document found in the stream
 */
function yaml_parse_url($url, $pos = 0, &$ndocs = 0, array $callbacks = null) {}

/**
 * Generate a YAML representation of the provided $data.
 * 
 * @link http://php.net/manual/en/functions.yaml-emit.php
 * @param mixed $data The $data being encoded. Can be any type except a resource
 * @param int $encoding Output character encoding chosen from YAML_ANY_ENCODING,
 *        YAML_UTF8_ENCODING, YAML_UTF16LE_ENCODING, YAML_UTF16BE_ENCODING
 * @param int $linebreak Output linebreak style chosen from YAML_ANY_BREAK,
 *        YAML_CR_BREAK, YAML_LN_BREAK, YAML_CRLN_BREAK
 * @param array $callbacks Content handlers for emitting YAML nodes. Associative
 *        array of classname = callable mappings. See emit callbacks for more details
 * @return string Returns a YAML encoded string on success
 */
function yaml_emit($data, $encoding = YAML_ANY_ENCODING, $linebreak = YAML_ANY_BREAK, array $callbacks = null) {}

/**
 * Generate a YAML representation of the provided $data in the $filename.
 * 
 * @link http://php.net/manual/en/functions.yaml-emit-file.php
 * @param string $filename Path to the file
 * @param mixed $data The $data being encoded. Can be any type except a resource
 * @param int $encoding Output character encoding chosen from YAML_ANY_ENCODING,
 *        YAML_UTF8_ENCODING, YAML_UTF16LE_ENCODING, YAML_UTF16BE_ENCODING
 * @param int $linebreak Output linebreak style chosen from YAML_ANY_BREAK,
 *        YAML_CR_BREAK, YAML_LN_BREAK, YAML_CRLN_BREAK
 * @param array $callbacks Content handlers for emitting YAML nodes. Associative
 *        array of classname = callable mappings. See emit callbacks for more details
 * @return bool Returns true on success
 */
function yaml_emit_file($filename, $data, $encoding = YAML_ANY_ENCODING, $linebreak = YAML_ANY_BREAK, array $callbacks = null) {}



?>
